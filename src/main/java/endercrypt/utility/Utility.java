package endercrypt.utility;


import java.io.IOException;

import com.googlecode.lanterna.TerminalPosition;
import com.googlecode.lanterna.input.KeyStroke;
import com.googlecode.lanterna.input.KeyType;

import endercrypt.Main;


public class Utility
{
	public static boolean isKeypress(KeyStroke keyStroke, char c)
	{
		if (isKeypress(keyStroke, KeyType.Character))
		{
			return Character.toLowerCase(keyStroke.getCharacter()) == Character.toLowerCase(c);
		}
		return false;
	}
	
	public static boolean isKeypress(KeyStroke keyStroke, KeyType keyType)
	{
		return keyStroke.getKeyType() == keyType;
	}
	
	public static void managedCall(RunnableException runnable)
	{
		try
		{
			runnable.run();
		}
		catch (Exception e)
		{
			crash(e);
		}
	}
	
	private static void shutdownTerminal() throws IOException
	{
		Main.terminalScreen.setCursorPosition(new TerminalPosition(0, 0));
		Main.terminalScreen.stopScreen();
		Main.terminalScreen.close();
	}
	
	public static void exit()
	{
		try
		{
			shutdownTerminal();
		}
		catch (IOException e2)
		{
			crash(e2);
		}
		System.exit(0);
	}
	
	public static void crash(Throwable e)
	{
		crash(e, 1);
	}
	
	public static void crash(Throwable e, int status)
	{
		try
		{
			shutdownTerminal();
		}
		catch (IOException e2)
		{
			// ignore
		}
		e.printStackTrace();
		System.exit(status);
	}
}
