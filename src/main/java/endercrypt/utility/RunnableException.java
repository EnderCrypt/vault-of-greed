package endercrypt.utility;

@FunctionalInterface
public interface RunnableException
{
	public void run() throws Exception;
}
