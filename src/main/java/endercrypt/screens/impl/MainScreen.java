package endercrypt.screens.impl;


import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.graphics.TextGraphics;
import com.googlecode.lanterna.input.KeyStroke;

import endercrypt.entities.Player;
import endercrypt.map.GameMap;
import endercrypt.map.MapTile;
import endercrypt.screens.Screen;
import endercrypt.screens.ScreenManager;
import endercrypt.utility.Utility;


public class MainScreen implements Screen
{
	private static Player player;
	
	public MainScreen()
	{
		GameMap startingMap = new GameMap(100, 100);
		
		player = new Player();
		player.move(startingMap, 50, 50);
	}
	
	@Override
	public void keypress(KeyStroke keyStroke)
	{
		if (Utility.isKeypress(keyStroke, 'i'))
		{
			ScreenManager.push(new InventoryScreen());
		}
	}
	
	@Override
	public void draw(TextGraphics graphics)
	{
		TerminalSize screenSize = graphics.getSize();
		int game_window_width = screenSize.getColumns() - 10;
		int game_window_height = screenSize.getRows() - 2;
		
		MapTile tile = player.getTile();
		GameMap map = tile.getMap();
		
		int map_x = tile.getX() - (game_window_width / 2);
		int map_y = tile.getY() - (game_window_height / 2);
		
		map.draw(graphics, map_x, map_y, 1, 1, game_window_width, game_window_height);
	}
}
