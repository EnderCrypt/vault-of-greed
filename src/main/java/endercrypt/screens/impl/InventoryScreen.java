package endercrypt.screens.impl;


import com.googlecode.lanterna.graphics.TextGraphics;
import com.googlecode.lanterna.input.KeyStroke;
import com.googlecode.lanterna.input.KeyType;

import endercrypt.entities.Player;
import endercrypt.map.GameMap;
import endercrypt.screens.Screen;
import endercrypt.screens.ScreenManager;
import endercrypt.utility.Utility;


public class InventoryScreen implements Screen
{
	private static Player player;
	
	public InventoryScreen()
	{
		GameMap startingMap = new GameMap(100, 100);
		
		player = new Player();
		player.move(startingMap, 50, 50);
	}
	
	@Override
	public void keypress(KeyStroke keyStroke)
	{
		if (Utility.isKeypress(keyStroke, KeyType.Escape))
		{
			ScreenManager.remove(this);
		}
	}
	
	@Override
	public void draw(TextGraphics graphics)
	{
		graphics.putString(1, 1, "Inventory");
	}
}
