package endercrypt.screens;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import endercrypt.utility.Utility;


public class ScreenManager
{
	private static List<Screen> screens = new ArrayList<Screen>();
	
	public static synchronized void push(Screen screen)
	{
		Objects.requireNonNull(screen);
		
		screens.add(screen);
	}
	
	public static synchronized void remove(Screen screen)
	{
		Objects.requireNonNull(screen);
		
		screens.remove(screen);
	}
	
	public static synchronized boolean hasScreens()
	{
		return screens.size() > 0;
	}
	
	public static synchronized Screen getTopScreen()
	{
		if (hasScreens() == false)
		{
			Utility.exit();
		}
		return screens.get(screens.size() - 1);
	}
}
