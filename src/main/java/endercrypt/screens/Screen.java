package endercrypt.screens;


import com.googlecode.lanterna.graphics.TextGraphics;
import com.googlecode.lanterna.input.KeyStroke;


public interface Screen
{
	public void keypress(KeyStroke keyStroke);
	
	public void draw(TextGraphics graphics);
}
