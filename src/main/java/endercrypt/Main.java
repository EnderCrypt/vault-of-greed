package endercrypt;


import java.io.IOException;
import java.util.Objects;

import javax.swing.JFrame;

import com.googlecode.lanterna.graphics.TextGraphics;
import com.googlecode.lanterna.input.KeyStroke;
import com.googlecode.lanterna.screen.TerminalScreen;
import com.googlecode.lanterna.terminal.DefaultTerminalFactory;
import com.googlecode.lanterna.terminal.Terminal;
import com.googlecode.lanterna.terminal.TerminalFactory;
import com.googlecode.lanterna.terminal.swing.SwingTerminalFrame;

import endercrypt.screens.Screen;
import endercrypt.screens.ScreenManager;
import endercrypt.screens.impl.MainScreen;
import endercrypt.utility.Utility;


public class Main
{
	public static TerminalScreen terminalScreen;
	
	public static void main(String[] args) throws IOException, InterruptedException
	{
		// terminal factory
		TerminalFactory terminalFactory = new DefaultTerminalFactory();
		
		// create terminal
		Terminal terminal = terminalFactory.createTerminal();
		
		// make swing terminal exit on (X)
		if (terminal instanceof SwingTerminalFrame)
		{
			SwingTerminalFrame swingTerminalFrame = (SwingTerminalFrame) terminal;
			swingTerminalFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		}
		
		// create terminal screen
		terminalScreen = new TerminalScreen(terminal);
		
		// hide cursor
		terminalScreen.setCursorPosition(null);
		
		// enter private mode
		terminalScreen.startScreen();
		
		// create game screen
		Utility.managedCall(() -> {
			ScreenManager.push(new MainScreen());
		});
		
		// resize listener
		terminal.addResizeListener((t, size) -> {
			update();
		});
		
		// main loop
		while (true)
		{
			update();
			
			KeyStroke keystroke = terminal.readInput();
			Objects.requireNonNull(keystroke);
			Utility.managedCall(() -> {
				ScreenManager.getTopScreen().keypress(keystroke);
			});
		}
	}
	
	private static void update()
	{
		draw(ScreenManager.getTopScreen());
	}
	
	private static synchronized void draw(Screen screen)
	{
		try
		{
			terminalScreen.clear();
			
			// resize
			terminalScreen.doResizeIfNecessary();
			
			// draw
			TextGraphics graphics = terminalScreen.newTextGraphics();
			Utility.managedCall(() -> {
				screen.draw(graphics);
			});
			
			terminalScreen.refresh();
		}
		catch (IOException e)
		{
			Utility.crash(e);
		}
	}
}
