package endercrypt.map;


import com.googlecode.lanterna.graphics.TextGraphics;


public class GameMap
{
	private final int width;
	private final int height;
	
	private MapTile[][] tiles;
	
	public GameMap(int width, int height)
	{
		this.width = width;
		this.height = height;
		
		this.tiles = new MapTile[width][height];
		for (int y = 0; y < height; y++)
		{
			for (int x = 0; x < width; x++)
			{
				this.tiles[x][y] = new MapTile(this, x, y);
			}
		}
	}
	
	public int getWidth()
	{
		return width;
	}
	
	public int getHeight()
	{
		return height;
	}
	
	public MapTile getTile(int x, int y)
	{
		if ((x < 0) || (y < 0) || (x >= getWidth()) || (y >= getHeight()))
		{
			return null;
		}
		else
		{
			return this.tiles[x][y];
		}
	}
	
	public void draw(TextGraphics graphics, int map_x, int map_y, int screen_x, int screen_y, int width, int height)
	{
		for (int y = 0; y < height; y++)
		{
			for (int x = 0; x < width; x++)
			{
				int local_map_x = map_x + x;
				int local_map_y = map_y + y;
				int local_screen_x = screen_x + x;
				int local_screen_y = screen_y + y;
				
				MapTile tile = getTile(local_map_x, local_map_y);
				if (tile == null)
				{
					graphics.setBackgroundColor(MapTile.DEFAULT_COLOR);
					graphics.setCharacter(local_screen_x, local_screen_y, ' ');
				}
				else
				{
					tile.draw(graphics, local_screen_x, local_screen_y);
				}
			}
		}
	}
}
