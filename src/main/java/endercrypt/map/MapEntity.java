package endercrypt.map;


import java.util.Objects;

import com.googlecode.lanterna.TextColor;


public class MapEntity
{
	// POSITION //
	
	MapTile tile;
	
	public MapTile getTile()
	{
		return tile;
	}
	
	public GameMap getMap()
	{
		MapTile tile = getTile();
		if (tile == null)
		{
			return null;
		}
		else
		{
			return tile.getMap();
		}
	}
	
	public boolean isActive()
	{
		return getTile() != null;
	}
	
	public void move(int x, int y)
	{
		if (isActive() == false)
		{
			throw new EntityMisplacementException("cant move inactive entity");
		}
		move(getMap(), x, y);
	}
	
	public void move(GameMap map, int x, int y)
	{
		Objects.requireNonNull(map);
		if (isActive())
		{
			delete();
		}
		tile = map.getTile(x, y);
		tile.getEditableEntities().add(this);
	}
	
	public void delete()
	{
		if (isActive() == false)
		{
			throw new EntityMisplacementException("cant dlete inactive entity");
		}
		getTile().getEditableEntities().remove(this);
		tile = null;
	}
	
	// COLOR //
	
	private static TextColor DEFAULT_COLOR = TextColor.Indexed.fromRGB(255, 255, 255);
	private static TextColor DEFAULT_BACKGROUND_COLOR = null;
	
	char character;
	TextColor color = DEFAULT_COLOR;
	TextColor backgroundColor = DEFAULT_BACKGROUND_COLOR;
	
	public void setEntityCharacter(char character, TextColor color, TextColor backgroundColor)
	{
		this.character = character;
		this.color = color;
		this.backgroundColor = backgroundColor;
	}
}
