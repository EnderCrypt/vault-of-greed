package endercrypt.map;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.googlecode.lanterna.TerminalPosition;
import com.googlecode.lanterna.TextColor;
import com.googlecode.lanterna.graphics.TextGraphics;


public class MapTile
{
	public static TextColor DEFAULT_COLOR = TextColor.Indexed.fromRGB(0, 0, 0);
	
	private final GameMap map;
	private final TerminalPosition position;
	
	private List<MapEntity> entities = new ArrayList<MapEntity>();
	
	public MapTile(GameMap map, int x, int y)
	{
		this(map, new TerminalPosition(x, y));
	}
	
	public MapTile(GameMap map, TerminalPosition position)
	{
		this.map = map;
		this.position = position;
	}
	
	public GameMap getMap()
	{
		return map;
	}
	
	List<MapEntity> getEditableEntities()
	{
		return entities;
	}
	
	public List<MapEntity> getEntities()
	{
		return Collections.unmodifiableList(getEditableEntities());
	}
	
	public TerminalPosition getPosition()
	{
		return position;
	}
	
	public int getX()
	{
		return getPosition().getColumn();
	}
	
	public int getY()
	{
		return getPosition().getRow();
	}
	
	public void draw(TextGraphics graphics, int x, int y)
	{
		char character = ' ';
		TextColor color = DEFAULT_COLOR;
		TextColor backgroundColor = DEFAULT_COLOR;
		
		// determine background color
		for (MapEntity entity : entities)
		{
			if (entity.backgroundColor != null)
			{
				backgroundColor = entity.backgroundColor;
			}
		}
		
		// draw
		if (entities.size() > 0)
		{
			MapEntity entity = entities.get(entities.size() - 1);
			character = entity.character;
			color = entity.color;
		}
		graphics.setForegroundColor(color);
		graphics.setBackgroundColor(backgroundColor);
		graphics.setCharacter(x, y, character);
	}
}
