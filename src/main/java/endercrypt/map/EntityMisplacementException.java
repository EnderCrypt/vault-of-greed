package endercrypt.map;

public class EntityMisplacementException extends RuntimeException
{
	
	public EntityMisplacementException()
	{
		// TODO Auto-generated constructor stub
	}
	
	public EntityMisplacementException(String message)
	{
		super(message);
		// TODO Auto-generated constructor stub
	}
	
	public EntityMisplacementException(Throwable cause)
	{
		super(cause);
		// TODO Auto-generated constructor stub
	}
	
	public EntityMisplacementException(String message, Throwable cause)
	{
		super(message, cause);
		// TODO Auto-generated constructor stub
	}
	
	public EntityMisplacementException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace)
	{
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}
	
}
