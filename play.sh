#!/bin/bash

cd $(dirname $0)

echo "building..."
mvn package

cd target

echo "playing!"
java -jar vault-of-greed-0.0.1-SNAPSHOT.jar

echo "exited with status: $?"
